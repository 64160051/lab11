package com.thatchai.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.walk();
        bird1.run();
        Plane boeing = new Plane("Boeig", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Superman clark = new Superman("clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();
        Bat bat1 = new Bat("Bat");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();
        bat1.walk();
        bat1.run();
        Rat rat1 = new Rat("Rat");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();
        Dog dog1 = new Dog("Dog");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        Cat cat1 = new Cat("Cat");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();
        Snake snake1 = new Snake("Snake");
        snake1.eat();
        snake1.sleep();
        snake1.crawl();
        Crocodile crocodile1 = new Crocodile("Crocodile");
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.crawl();
        Fish fish1 = new Fish("Fish");
        fish1.eat();
        fish1.sleep();
        fish1.swim();


        Flyable[] flyables = {bird1, boeing, clark, bat1};
        for(int i=0; i<flyables.length;i++){
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        Walkable[] walkables = {bird1, clark, man1, bat1, rat1, dog1, cat1};
        for(int i=0; i<walkables.length;i++){
            walkables[i].walk();
            walkables[i].run();
        }
        Crawlable[] crawlables = {snake1, crocodile1};
        for(int i=0; i<crawlables.length;i++){
            crawlables[i].crawl();
        }
        Swimable[] swimables = {fish1};
        for(int i=0; i<swimables.length;i++){
            swimables[i].swim();
        }
    }
}
